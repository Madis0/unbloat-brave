# Unbloat Brave

A list of [group policy admin templates](https://support.brave.com/hc/en-us/articles/360039248271-Group-Policy) that disable some bloat of [Brave browser](brave.com/).

## FAQ

**Why?** 

For some features, Brave will not give the normal users enough options to disable every part of a feature, especially Rewards. Having a group policy ensures that the feature will always be disabled in its entirety, no marketing to re-enable etc.

**Supported OS?**

Currently Linux distributions only. Windows will come later and I'm not sure if macOS is possible [without external software...](https://support.google.com/chrome/a/answer/9020078?hl=en&ref_topic=7650028)

**Minimum Brave version?**

1.45.0

**Why so few options?**

[Brave is still working on adding more.](https://github.com/brave/brave-browser/issues/22029)

**Side effects?**

Well, you can't toggle those options from the GUI anymore, should you want that. And you'll get the "managed by your organization" label on menu, settings etc.

## Disabled features

* [Brave Rewards](https://support.brave.com/hc/en-us/articles/360027276731-Brave-Rewards-FAQ)
* [Tor](https://support.brave.com/hc/en-us/articles/360018121491-What-is-a-Private-Window-with-Tor-Connectivity-)
* [IPFS](https://support.brave.com/hc/en-us/articles/360051406452-How-does-IPFS-Impact-my-Privacy-)

## Installation

### Linux

Execute this in the folder you downloaded `test_policy.json` to:
```
sudo mkdir --parents /etc/brave/policies/managed/; sudo mv test_policy.json $_
```
Admin permissions required. The command will ask for the admin user's password.

## Uninstallation

### Linux

Execute this anywhere:

```
sudo rm /etc/brave/policies/managed/test_policy.json
```

Admin permissions required. The command will ask for the admin user's password.
